"use strict";

let operatorState = false;
let firstNumber = document.querySelector('.firstNum');
let operatorView = document.querySelector('.operator');
let secondNumber = document.querySelector('.secondNum');
let equals = document.getElementById('equals');
let clear = document.getElementById('clear');
let posNeg = document.getElementById('posNeg');
let classname = document.getElementsByClassName('num');
let operator = document.getElementsByClassName('op');
let sRoot = document.getElementById('sqrt');

for (let i=0; i < classname.length; i++){
    classname[i].addEventListener('click', append, false);
}

for (let i=0; i < operator.length; i++){
    operator[i].addEventListener('click', appendOp, false);
}

function append(){
    if(operatorState===false){
        firstNumber.textContent += this.value;
    } else {
        secondNumber.textContent += this.value;
    }
}

function appendOp(){
    operatorState = true;
    operatorView.textContent = this.value;
}

posNeg.addEventListener('click', (event) => {
    if(operatorState===false){
        if (parseFloat(firstNumber.textContent) > 0) {
            let signedNumber = firstNumber.textContent.split('');
            signedNumber.unshift('-');
            firstNumber.textContent = signedNumber.join('');
        } else {
            let signedNumber = firstNumber.textContent.split('');
            signedNumber.shift();
            firstNumber.textContent = signedNumber.join('');
        }
    } else {
        if (parseFloat(secondNumber.textContent) > 0) {
            let signedNumber = secondNumber.textContent.split('');
            signedNumber.unshift('-');
            secondNumber.textContent = signedNumber.join('');
        } else {
            let signedNumber = secondNumber.textContent.split('');
            signedNumber.shift();
            secondNumber.textContent = signedNumber.join('');
        }
    }
});

function resolve() {
    switch(operatorView.textContent){
        case '+':
            firstNumber.textContent = (parseFloat(firstNumber.textContent) + parseFloat(secondNumber.textContent)).toPrecision(4);
            break;
        case '-':
            firstNumber.textContent = parseFloat(firstNumber.textContent) - parseFloat(secondNumber.textContent);
            break;
        case '*':
            firstNumber.textContent = parseFloat(firstNumber.textContent) * parseFloat(secondNumber.textContent);
            break;
        case '/':
            firstNumber.textContent = (parseFloat(firstNumber.textContent) / parseFloat(secondNumber.textContent)).toPrecision(4);
            break;
    }
    secondNumber.textContent = '';
    operatorView.textContent = '';
}

sRoot.addEventListener('click', (event) => {
    resolve();
    let rootNumber = parseFloat(firstNumber.textContent);
    firstNumber.textContent = Math.sqrt(rootNumber).toPrecision(4);
});

equals.addEventListener('click', resolve, false);

clear.addEventListener('click', (event) => {
   firstNumber.textContent = '';
   secondNumber.textContent = '';
   operatorView.textContent = '';
   operatorState = false;
});


